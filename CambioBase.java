package progra.modular;

import java.util.Scanner;

public class CambioBase {

	static Scanner entry = new Scanner (System.in);
	
	public static void cambioBase(int numero, int new_base) {
		if (numero < new_base) {
			System.out.print(numero);
		} else {
			cambioBase(numero/new_base, new_base);
			System.out.print(numero%new_base);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Introduce numero:");
		int num = entry.nextInt();
		System.out.println("Introduce nueva base:");
		int base = entry.nextInt();
		
		System.out.println("Resultado:");
		cambioBase(num, base);
	}
}
