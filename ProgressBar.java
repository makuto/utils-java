package progra.modular;

public class ProgressBar {

	public static void main(String[] args) throws InterruptedException {
		for (int i=0; i<100; i++) {
			for (int j=0; j<i; j++) {
				System.out.print("#");
			}
			System.out.print("\r");
			Thread.sleep(500);
		}

	}

}
