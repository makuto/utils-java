package progra.modular;

public class Mates {

	public static int add1(int n) {
		if (n > 0)
			n++;
		else if (n < 0)
			n--;
		return n;
	}

	public static int sub1(int n) {
		if (n > 0)
			n--;
		else if (n < 0)
			n++;
		return n;
	}

	public static double elevar(long n, int m) {
		if (m > 0) {
			return n * elevar(n, m - 1);
		} else if (m < 0) {
			return elevar(n, m + 1) / n;
		} else
			return 1;
	}

	public static long elevarx(long n, int m) {
		long r = n;
		for (m--; m > 0; m--) {
			r *= n;
		}
		return r;
	}
	
	public static long factorial(int n) {
		if (n > 0) {
			return n * factorial(n - 1);
		} else if (n == 0) {
			return 1;
		} else
			return 0;
	}

	public static long permutacion(int n) {
		return factorial(n);
	}

	public static long combinacion(int m, int n) {
		return factorial(m) / (factorial(n) * factorial(m - n));
	}

	public static long variacion(int m, int n) {
		return factorial(m) / factorial(m - n);
	}

	public static double truncar(double n, int ndec) {
		long e = elevarx(10, ndec);
		long inum = (long) (n * e);
		return (double) inum / e;
	}

	public static void mainTest() {
		System.out.println(elevar(6, 4));
		System.out.println(elevar(5, -3));
		System.out.println(truncar(3485.456543, 4));
	}

    // Modificado 8 nov 19

}
